"""
Websocket routing configuration for the {{ cookiecutter.project_slug }} project.

It exposes the `websocket_urlpatterns` list, a list of url patterns to be used
for deployment.

See Also
--------
https://channels.readthedocs.io/en/stable/topics/routing.html
"""

from typing import List, Any

import dasf_broker.routing as dasf_routing
from channels.routing import URLRouter
from dasf_broker.app_settings import DASF_WEBSOCKET_URL_ROUTE

from django.urls import path  # noqa: F401

websocket_urlpatterns: List[Any] = [
    path(
        DASF_WEBSOCKET_URL_ROUTE,
        URLRouter(dasf_routing.websocket_urlpatterns),
    ),
]
